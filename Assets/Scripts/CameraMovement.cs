﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraMovement : MonoBehaviour 
{

	public float perspectiveZoomSpeed = 0.5f;
	public float scrollSpeed = 0.5f;

	Touch touchZero;
	Touch touchOne;

	Vector3 lastMousePosition = Vector3.zero;

	Transform targetPlanet;
	Vector3 targetOffset;

	bool canPan = true;

	float smoothSpeed = 0.1f;

	bool isOverUI = false;

	// Update is called once per frame
	void Update () 
	{
		isOverUI = EventSystem.current.IsPointerOverGameObject(); //Check to see if the mouse is over anything
		if (canPan)
		{
			switch (Input.touchCount)
			{
				case 1:
					{
						if (Input.touches [0].phase == TouchPhase.Began && isOverUI)
							break;
						else
							cameraPanTouch ();
						break;
					}
				case 2:
					{
						if (Input.touches [0].phase == TouchPhase.Began && isOverUI)
							break;
						else
							cameraZoomTouch ();
						break;
					}
			}

			if (Input.GetMouseButton (0))
			{
				if (isOverUI)
				{
				}
				else
					cameraPan ();
			} else
			{
				lastMousePosition = Vector3.zero;
			}

			//Move camera up and down using scroll wheel
			transform.localPosition = transform.localPosition = new Vector3 (transform.position.x, transform.position.y + Input.mouseScrollDelta.y, transform.position.z);

			//Stop us from zooming too far in or out and panning too far out
			transform.localPosition = new Vector3 (Mathf.Clamp (transform.localPosition.x, -500, 500), Mathf.Clamp (transform.localPosition.y, 1f, 999f), transform.localPosition.z);
		}
	}

	void FixedUpdate()
	{
		if (targetPlanet != null)
		{
			Vector3 desiredPosition = targetPlanet.position + targetOffset;
			Vector3 smoothedPosition = Vector3.Lerp (transform.position, desiredPosition, smoothSpeed);
			transform.position = smoothedPosition;
		}
	}

	void cameraPan()
	{
		//Could use Input.GetMouseButtonDown(0) to set this
		if (lastMousePosition != Vector3.zero)
		{
			//Get the movement distance between the last frame and this frame mouse position
			Vector3 prevMousePosition = (Input.mousePosition - lastMousePosition);
			prevMousePosition = new Vector3 (prevMousePosition.x, 0, prevMousePosition.y);
			transform.position -= (prevMousePosition/20);
		}
		targetPlanet = null;
		lastMousePosition = Input.mousePosition;
	}

	void cameraPanTouch()
	{
		targetPlanet = null;
	}

	void cameraZoomTouch()
	{
		touchZero = Input.GetTouch (0);
		touchOne = Input.GetTouch (1);
		targetPlanet = null;
		//Find out the position from the previous frame
		Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
		Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

		//Calculate the distance travelled from the previous frame in magnitude
		float prevTouchDeltaMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;

		//Calculate the distance between touches in this frame
		float touchDeltaMagnitude = (touchZero.position - touchOne.position).magnitude;

		//Find the differentce in the distances between the previous and current frame
		float deltaMagnitudeDifference = prevTouchDeltaMagnitude - touchDeltaMagnitude;

		//Then we change the field of view based on this difference
		transform.position = new Vector3(transform.position.x, transform.position.y + (deltaMagnitudeDifference/3), transform.position.z);
	}

	public void followPlanet(Transform planet)
	{
		//Set the planet for our Camera to follow
		targetPlanet = planet;
		targetOffset = new Vector3 (0, Mathf.Clamp(planet.localScale.y*2,3,40), -planet.localScale.z/2);
	}

	public void smoothMoveToPlanet(Planet planet = null)
	{
		targetPlanet = planet.transform;

		//Set how quickly we'll transition to the new planet to give a sense of distance
		smoothSpeed = planet.getTransitionTime();

		if (GameManager.instance.getDisplayMode() == 2) //Scaled mode
		{
			targetOffset = new Vector3 (0, 0, (-planet.transform.localScale.z*1.5f)-1);
		}
		else if (GameManager.instance.getDisplayMode() == 3) //Scaled mode
		{
			targetOffset = new Vector3 ((planet.transform.localScale.x*1.5f)+1, 0, 0);
		}
	}

	public void togglePanning(bool value)
	{
		canPan = value;
		if (value)
			targetPlanet = null;
	}
}
