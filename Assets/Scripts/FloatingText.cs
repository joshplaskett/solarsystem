﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingText : MonoBehaviour {

	RectTransform rectTrans;
	public Transform target;
	private Vector2 uiOffset;
	// Use this for initialization
	void Start () {
		rectTrans = GetComponent<RectTransform> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		rectTrans.anchoredPosition = Camera.main.WorldToScreenPoint (target.position.normalized);
	}
}
