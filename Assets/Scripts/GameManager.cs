﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager instance;

	//Store a reference to all our planets so we can toggle their display mode
	public Planet[] planets;

	enum DisplayMode
	{
		Orbit=1, Scale=2, Distance=3
	};

	DisplayMode currentDisplayMode = DisplayMode.Orbit; //Start off orbiting

	[SerializeField]
	Button planetInfoButton;
	[SerializeField]
	GameObject planetInfo;

	Planet currentPlanet;

	//Our custscene alien
	[SerializeField]
	LocalisedText speechBubble;

	[SerializeField]
	Text speedLabel;

	//Our Planet Statistics UI
	[SerializeField]
	LocalisedText title;

	[SerializeField]
	GameObject planetListPanel;
	[SerializeField]
	GameObject planetListPanelCloseButton;
	[SerializeField]
	Animator QuitMenu;
	[SerializeField]
	GameObject planetCycler;

	[SerializeField]
	LocalisedText planetCycleButtonText;

	[SerializeField]
	GameObject orbits;

	[SerializeField]
	Text gravity, aveTemp, moons;

	void Awake () 
	{
		//If there is no other game manager, set it to this one
		if (instance == null)
		{
			instance = this;
		} else if(instance != this)
		{
			//Otherwise, one already exists (that isn't this one), so destroy this one
			Destroy (gameObject);
		}
		//Make sure this persists
		DontDestroyOnLoad (gameObject);
	}

	void Start()
	{
		//To help with testing, if you play this scene without having selected a language, take us back to language selection
		if (LocalisationManager.instance == null)
		{
			Destroy (gameObject);
			SceneManager.LoadScene ("MainMenu");
		} else
		{
			speechBubble.changeKey ("intro");
		}
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			QuitMenu.SetTrigger ("Show");
		}
	}

	public void setSpeed(Slider slider)
	{
		Time.timeScale = slider.value;
		speedLabel.text = Mathf.Round(slider.value).ToString () + "x";
	}

	public void toggleDisplay()
	{
		//Check what current display state we're in and toggle to the next display mode
		switch (currentDisplayMode)
		{
			case DisplayMode.Orbit:
				{
					
					Camera.main.GetComponent<CameraMovement> ().togglePanning (false);
					currentDisplayMode = DisplayMode.Scale;
					//Organise our planet positions
					Vector3 firstPlanetLocation = new Vector3(-30,5,30);
					for (int i = 0; i < planets.Length; i++)
					{
						if (i == 0)
						{
							//Set the first planet at our designated starting point
							planets [i].toggleDisplayMode ((int)currentDisplayMode, firstPlanetLocation);
						}
						else
						{
							//Get the previous planet's position and scale, and use this to place this planet next to it
							Vector3 newPosition;
							newPosition = planets [i - 1].transform.position + new Vector3 (planets [i - 1].transform.localScale.x / 2, 0) + new Vector3(planets[i].transform.localScale.x/1.7f,0);
							planets [i].toggleDisplayMode ((int)currentDisplayMode, newPosition); //Set it at the start of the display
						}
					}
					Camera.main.transform.rotation = Quaternion.Euler(Vector3.zero);
					Camera.main.GetComponent<CameraMovement>().smoothMoveToPlanet(planets[0]);
					currentPlanet = planets [0];
					planetCycleButtonText.changeKey (currentPlanet.getPlanetNameKey ());
					//Now move our camera, positioning it a little back from the first little planet, Mercury
					Camera.main.transform.rotation = Quaternion.Euler(Vector3.zero);
					//Camera.main.transform.SetPositionAndRotation(firstPlanetLocation + new Vector3(0,0,-5),Quaternion.Euler(Vector3.zero));
					planetCycler.GetComponent<Animator>().SetTrigger("Show");
					if(planetInfoButton.animator.GetCurrentAnimatorClipInfo (0)[0].clip.name.Contains("Show"))
						planetInfoButton.animator.SetTrigger ("Hide");
					if (!planetListPanel.GetComponent<Animator> ().GetCurrentAnimatorClipInfo (0) [0].clip.name.Contains ("Hidden"))
					{
						if(planetListPanel.GetComponent<Animator> ().GetCurrentAnimatorClipInfo (0) [0].clip.name.Contains ("Open"))
							planetListPanelCloseButton.GetComponent<Button> ().onClick.Invoke ();
						planetListPanel.GetComponent<Animator> ().SetTrigger ("Hide");
					}
					orbits.SetActive (false);
					break;
				}
			case DisplayMode.Scale:
				{

					Camera.main.GetComponent<CameraMovement> ().togglePanning (false);
					currentDisplayMode = DisplayMode.Distance;
					foreach (Planet planet in planets)
					{
						planet.toggleDisplayMode ((int)currentDisplayMode, Vector3.zero);
					}

					//Now move our camera, positioning it a little to the right from the first little planet, Mercury

					//Camera.main.transform.SetPositionAndRotation(planets[0].transform.position + new Vector3(5,0,0),Quaternion.Euler(new Vector3(0,-90,0)));
					Camera.main.GetComponent<CameraMovement>().smoothMoveToPlanet(planets[0]);
					Camera.main.transform.rotation = Quaternion.Euler(new Vector3(0,-90,0));
					currentPlanet = planets [0];
					planetCycleButtonText.changeKey (currentPlanet.getPlanetNameKey ());
					if(planetInfoButton.animator.GetCurrentAnimatorClipInfo (0)[0].clip.name.Contains("Show"))
						planetInfoButton.animator.SetTrigger ("Hide");
					
					if (!planetListPanel.GetComponent<Animator> ().GetCurrentAnimatorClipInfo (0) [0].clip.name.Contains ("Hidden"))
					{
						if(planetListPanel.GetComponent<Animator> ().GetCurrentAnimatorClipInfo (0) [0].clip.name.Contains ("Open"))
							planetListPanelCloseButton.GetComponent<Button> ().onClick.Invoke ();
						planetListPanel.GetComponent<Animator> ().SetTrigger ("Hide");
					}
					orbits.SetActive (false);
					break;
				}
			case DisplayMode.Distance:
				{
					currentDisplayMode = DisplayMode.Orbit;
					Camera.main.GetComponent<CameraMovement> ().togglePanning (true);
					foreach (Planet planet in planets)
					{
						planet.toggleDisplayMode ((int)currentDisplayMode, Vector3.zero);
					}

					//Now move our camera, positioning it opverhead of the Sun
					Camera.main.transform.SetPositionAndRotation(new Vector3(0,50,0),Quaternion.Euler(new Vector3(90,0,0)));
					currentPlanet = null;
					planetCycler.GetComponent<Animator>().SetTrigger("Hide");
					if(planetListPanel.GetComponent<Animator>().GetCurrentAnimatorClipInfo (0)[0].clip.name.Contains("Hidden"))
						planetListPanel.GetComponent<Animator>().SetTrigger ("Show");
					orbits.SetActive (true);
					break;
				}
		}
	}

	/*public void planetMouseDown(Planet planet)
	{
		currentPlanet = planet;
		planetInfoButton.GetComponentInChildren<LocalisedText> ().changeKey (planet.getPlanetNameKey ());
	}*/

	public void planetClicked(Planet planet)
	{
		//if (currentPlanet == planet)
		{
			string key = planet.getPlanetNameKey ().ToLower ();
			currentPlanet = planet;
			planet.visited = true;
			planetInfoButton.GetComponent<Animator> ().SetTrigger ("Show");
			speechBubble.changeKey (key + "_description");
			planetInfoButton.GetComponentInChildren<LocalisedText> ().changeKey (key);
		}
	}

	public void showPlanetInfo(Button button)
	{
		//Bring up our planet info panel and hide the planet info button
		planetInfoButton.GetComponent<Animator> ().SetTrigger ("Hide");
		//Populate the planet info panel with new correct information
		planetInfo.GetComponentInChildren<LocalisedText> ().changeKey (currentPlanet.getPlanetNameKey());
		title.changeKey (currentPlanet.getPlanetNameKey ());
		gravity.text = currentPlanet.gravity.ToString ();
		aveTemp.text = currentPlanet.aveTemp.ToString () + "°C";
		moons.text = currentPlanet.moons.Length.ToString ();
		planetInfo.GetComponent<Animator>().SetTrigger("Show");
	}

	public void cylceToNextPlanet()
	{
		for (int i = 0; i < planets.Length; i++)
		{
			if (planets [i].getPlanetNameKey() == currentPlanet.getPlanetNameKey())
			{
				if (i + 1 <= planets.Length)
				{
					Camera.main.GetComponent<CameraMovement> ().smoothMoveToPlanet (planets [i + 1]);
					currentPlanet = planets [i + 1];
					planetCycleButtonText.changeKey (currentPlanet.getPlanetNameKey ());
				}
				break;
			}
		}
	}

	public void cylceToPreviousPlanet()
	{
		for (int i = 0; i < planets.Length; i++)
		{
			if (planets [i].getPlanetNameKey() == currentPlanet.getPlanetNameKey())
			{
				if (i - 1 >= 0)
				{
					Camera.main.GetComponent<CameraMovement> ().smoothMoveToPlanet (planets [i - 1]);
					currentPlanet = planets [i - 1];
					planetCycleButtonText.changeKey (currentPlanet.getPlanetNameKey ());
				}
				break;
			}
		}
	}

	public void showCycledPlanetInfo()
	{
		planetInfo.GetComponentInChildren<LocalisedText> ().changeKey (currentPlanet.getPlanetNameKey());

		title.changeKey (currentPlanet.getPlanetNameKey ());
		gravity.text = currentPlanet.gravity.ToString ();
		aveTemp.text = currentPlanet.aveTemp.ToString () + "°C";
		moons.text = currentPlanet.moons.Length.ToString ();
		planetInfo.GetComponent<Animator>().SetTrigger("Show");
	}

	public int getDisplayMode()
	{
		return (int)currentDisplayMode;
	}

	public void quitGame()
	{
		Application.Quit ();
	}
}
