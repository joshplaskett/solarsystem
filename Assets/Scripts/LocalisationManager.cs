﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class LocalisationManager : MonoBehaviour 
{
	//Singleton design pattern
	public static LocalisationManager instance;

	private Dictionary<string, string> localisedText;
	private bool isReady = false;

	private string missingTextString = "Localised Text Not Found";

	public Text loadingText;

	string path;
	//Make sure this runs first
	void Awake () 
	{
		//If there is no other localisation manager, set it to this one
		if (instance == null)
		{
			instance = this;
		} else if(instance != this)
		{
			//Otherwise, one already exists (that isn't this one), so destroy this one
			Destroy (gameObject);
		}
		//Make sure this persists
		DontDestroyOnLoad (gameObject);
	}

	public void loadLocalisedText(string filename)
	{
		//Set up a new dictionary
		localisedText = new Dictionary<string,string> ();
		loadingText.text = "10%";
		//If we're on Android, we have to use WWW as it's stored in a .jar file
		if (Application.platform == RuntimePlatform.Android || Application.platform.ToString() == "Android")
		{
			StartCoroutine ("LoadLocalisedTextOnAndroid",filename);
			return;
		}

		//Get the actual text file
		string filePath = "";
		filePath = Path.Combine (Application.streamingAssetsPath, filename);
		//Confirm the file is present
		if (File.Exists (filePath))
		{
			//Read in all of the text from the file
			string dataAsJson = File.ReadAllText (filePath);
			//Insert into dictionary via array so Unity can serialize it for the inspector.
			LocalisationData loadedData = JsonUtility.FromJson<LocalisationData> (dataAsJson);
			//Loop through the data to populate the dictionary
			for (int i = 0; i < loadedData.items.Length; i++)
			{
				localisedText.Add (loadedData.items [i].key, loadedData.items [i].value);
			}
			//Test to confirm data is put into the dictionary
			Debug.Log ("Data loaded, dictionary contains: " + localisedText.Count + " entries");
		} else
		{
			loadingText.text = "Localistion file is missing";
			Debug.LogError ("Localisation file is missing");
		}
		loadingText.text = "100%";
		isReady = true;
	}

	IEnumerator LoadLocalisedTextOnAndroid(string filename)
	{
		//Get the actual text file
		string filePath;
		filePath = Path.Combine (Application.streamingAssetsPath + "/", filename);
		string dataAsJson;
		loadingText.text = "15%";
		//Check whether it contains the correct path
		if (filePath.Contains ("://") || filePath.Contains (":///")) 
		{
			loadingText.text = "UNITY:" + System.Environment.NewLine + filePath;
			//Use the WWW networking request to fetch the file
			UnityEngine.Networking.UnityWebRequest www = UnityEngine.Networking.UnityWebRequest.Get (filePath);
			yield return www.Send ();
			loadingText.text = "50%";
			dataAsJson = www.downloadHandler.text;
		} else 
		{
			//Read in all of the text from the file
			loadingText.text = "80%";
			dataAsJson = File.ReadAllText (filePath);
		}
		//Insert into dictionary via array so Unity can serialize it for the inspector.
		LocalisationData loadedData = JsonUtility.FromJson<LocalisationData> (dataAsJson);
		//Loop through the data to populate the dictionary
		for (int i = 0; i < loadedData.items.Length; i++) 
		{
			localisedText.Add (loadedData.items [i].key, loadedData.items [i].value);
		}
		Debug.Log ("Data loaded, dictionary contains: " + localisedText.Count + " entries");
		loadingText.text = "100%";
		isReady = true;
	}

	public string getLocalisedValue(string key)
	{
		//Initially set the result to the missing text string
		string result = missingTextString;
		//Check the dictionary to see if there is an item with the provided key
		if(localisedText.ContainsKey(key))
		{
			result = localisedText [key];
		}

		return result;
	}

	public bool getIsReady()
	{
		return isReady;
	}
}
