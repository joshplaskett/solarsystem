﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalisedText : MonoBehaviour {

	//This will be the key entered in inspector to look up in the dictionary
	public string key;

	void Start () 
	{
		//Get the UI Text component attached to this game object
		Text text = GetComponent<Text> ();
		//Fetch the localised text from the localisation manager
		if(LocalisationManager.instance)
			text.text = LocalisationManager.instance.getLocalisedValue (key);
	}

	public void changeKey(string newKey)
	{
		if (!LocalisationManager.instance)
			return;
		//Get the UI Text component attached to this game object
		Text text = GetComponent<Text> ();
		//Fetch the localised text from the localisation manager
		text.text = LocalisationManager.instance.getLocalisedValue (newKey);

		if (GetComponent<TypeWriterEffect> () != null)
		{
			GetComponent<TypeWriterEffect> ().TriggerNewText ();
		}
	}

}
