﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbits : MonoBehaviour {


	public List<float> orbitRadii;

	[Range(3, 256)]
	public int numSegments = 128;

	public Color color = new Color(1,1,1,0.5f);

	public Transform parentObject;
	// Use this for initialization

	void Awake () 
	{
		if (orbitRadii.Count <= 0)
		{
			for (int i = 0; i < Camera.main.GetComponent<GameManager> ().planets.Length; i++)
			{
				orbitRadii.Add (Camera.main.GetComponent<GameManager> ().planets [i].transform.position.z);
			}
		}
	}
	
	// Update is called once per frame
	void Start () {
		foreach (float radius in orbitRadii)
		{
			RenderNewLine (radius);
		}
	}

	public void RenderNewLine (float radius) 
	{
		//Create a new game object to host our renderer
		GameObject orbitGO = new GameObject();
		Vector3 origin = parentObject == null ? Vector3.zero : parentObject.transform.position;

		orbitGO.transform.parent = this.transform;
		LineRenderer lineRenderer = orbitGO.AddComponent<LineRenderer>();

		Color c1 = color;
		lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
		lineRenderer.startColor = c1;
		lineRenderer.endColor = c1;

		lineRenderer.startWidth = 0.2f;
		lineRenderer.endWidth = 0.2f;
		lineRenderer.positionCount = numSegments+1;

		lineRenderer.useWorldSpace = false;

		float deltaTheta = (float) (2.0 * Mathf.PI) / numSegments;
		float theta = 0f;

		for (int i = 0 ; i < numSegments + 1 ; i++) {
			float x = radius * Mathf.Cos(theta);
			float z = radius * Mathf.Sin(theta);
			Vector3 pos = new Vector3(x, 0, z);
			lineRenderer.SetPosition(i, pos);
			theta += deltaTheta;
		}
		//Reaffirm our starting position	
		orbitGO.transform.position = parentObject == null ? Vector3.zero : parentObject.transform.position;
	}
}
