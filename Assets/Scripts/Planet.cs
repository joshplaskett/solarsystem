﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour 
{
	//Each planet has a name, size, rotation speed, orbit speed, and statistics such as gravity, temperature and moons
	[SerializeField]
	string planetNameKey = "";

	public float gravity, aveTemp, lengthOfDay, lengthOfOrbit; //length of day is in hours, length of orbit is in Earth 

	[Range(0.01f,0.99f)]
	public float transitionTime = 0.125f;

	public string[] moons; //Moons are in an array of strings (names) to get the count and name of moons

	string planetType;

	Rigidbody rb;

	[SerializeField]
	GameObject planetUI;
	[SerializeField]
	Vector3 lastOrbitPosition;
	Vector3 startingPos; // Used to display the planets in distance mode
	Vector3 startingScale; //Used to display the planets at their normal scale
	float scaleFactor = 1;

	[SerializeField]
	bool orbiting = true;

	public bool visited = false;

	// Use this for initialization
	void Start () 
	{
		startingPos = transform.position;
		startingScale = transform.localScale;
		rb = GetComponent<Rigidbody> ();
		//Make our planet spin
		rb.AddRelativeTorque (Vector3.up * lengthOfDay);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (orbiting)
		{
			transform.RotateAround (Vector3.zero, Vector3.up, lengthOfOrbit * Time.deltaTime); //Make our planet orbit
			//If the Camera is zoomed out, scale the planet respectively
			//TODO the math behind this
			/*scaleFactor = Mathf.Clamp(Camera.main.transform.position.y/100,1f,100f);
			transform.localScale = startingScale * scaleFactor;*/
		} else if (transform.localScale.y > startingScale.y)
		{
			transform.localScale = startingScale;
		}
	}

	public string getPlanetNameKey()
	{
		return planetNameKey;
	}

	public void toggleDisplayMode(int displayMode, Vector3 displayPos)
	{
		switch (displayMode)
		{
			case 1:
				{
					returnToLastOrbitPosition ();
					orbiting = true;
					break;
				}
			case 2:
				{
					setLastOrbitPosition ();
					transform.position = displayPos;
					orbiting = false;
					break;
				}
			case 3:
				{
					transform.position = startingPos;
					orbiting = false;
					break;
				}
		}

	}

	void setLastOrbitPosition()
	{
		lastOrbitPosition = transform.position;
	}

	void returnToLastOrbitPosition()
	{
		transform.position = lastOrbitPosition;
	}

	public float getTransitionTime()
	{
		return transitionTime;
	}

	public void planetSelected()
	{
		if(orbiting)
			GameManager.instance.planetClicked (this);
	}
}
