﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartupManager : MonoBehaviour {

	// Use as a Coroutine
	private IEnumerator Start () 
	{
		//While the localisation data hasn't been loaded...
		while (!LocalisationManager.instance.getIsReady ())
		{
			//Wait until it has been loaded
			yield return null;
		}

		SceneManager.LoadScene ("SolarSystem");
	}
	

}
