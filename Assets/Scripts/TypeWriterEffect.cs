﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypeWriterEffect : MonoBehaviour {

	Text text;
	string description;

	public Animator anim;

	bool isAnimating = false;

	void Start () 
	{
		text = GetComponent<Text> ();
		description = text.text;
		text.text = "";

		StartCoroutine ("PlayText");
	}
	
	IEnumerator PlayText()
	{
		isAnimating = true;
		yield return new WaitForSeconds (1.5f); //Wait one second for the animations to complete
		foreach (char c in description)
		{
			if (c == char.Parse("~")) //This is our special character to start a new sentence
			{
				yield return new WaitForSeconds (1.75f); //Extra pause here to allow finishing reading
				text.text = "";
			} else
			{
				text.text += c;
				yield return new WaitForSeconds (0.05f);
			}
		}
		yield return new WaitForSeconds (2.5f); //Wait a few more seconds before calling an animation
		text.text = "";
		isAnimating = false;
		anim.SetTrigger ("Hide");

	}

	public void TriggerNewText()
	{
		text = GetComponent<Text> ();
		description = text.text;
		text.text = "";
		if(!isAnimating)
			anim.SetTrigger ("Show");
		else
			StopCoroutine ("PlayText");
		StartCoroutine ("PlayText");
	}
}
